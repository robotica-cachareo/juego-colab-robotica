label capitulo4:
    
   mt "Analizando código de tiempo. Comprobando algoritmo..."
   
   mt "Uno por ciento..."
   
   mt "Cincuenta y siete por ciento..."
   
   mt "Comprobación exitosa. Buscando lugar amplio para llegada..."
   
   mt "Iniciando viaje."
   
   mt "Destino: Atenas 31 de mayo de 430 a.C"
   
   al "¡Por fin! Tras tanto viaje vamos a llegar al destino..."
   
   so "Atenas... Creo que no estaban en su mejor momento."
   
   al "No la verdad. En guerra y enfermos. No es la mejor época para visitar la zona, pero tampoco nos queda otra."
   
   so "A mí con que no pase lo de Albania..."
   
   al "No, por favor. Lo de Albania no."
   
   scene atenas1 #Por definir este escenario
   
   al "Que depresión. La cuna de la civilización, y aquí solo hay ratas, pulgas y suciedad."
   
   so "No tendremos mucho tiempo para hacer turismo. Los de Nidavellir estarán a punto de escapar, la alarma saltó anteayer."
   
   al "Vamos. No deben andar muy lejos."
   
   pl "¡Tú! ¿Cuál es tu nombre?"
   
   al "Eh... Alejandro. ¿Por qué?"
   
   pl "Alejandro... ¿y qué se supone que haces sin tu equipo?"
   
   pl "Los espartanos asediando la ciudad y tú aquí haciendo el capullo con tu señora"
   
   pl "¡Vamos!"
   
   al "Eh... pero..."
   
   pl "Sígueme, vamos a la armería."
   
   so "Ve. Ya sabes lo que tienes que hacer. Yo me ocupo de los nidavellirios."
   
   al "Vale, vale."
   
   scene atenasarmeria #Escenario por definir
   
   pl "¿Eres hoplita o arquero, Alejandro?"
   
   al "¿Yo? Pues..."
   
   pl "¿Qué eres?"
   
   menu:
       
       "Hoplita":
          "Venga, coge la lanza y el escudo"
          jump combate_hoplita
          
       "Arquero":
          "Coge un arco y ya. No te hará falta más."
          jump combate_arquero
          
        
label combate_arquero:
    
    pl "El combate está a punto de empezar en Decelia. ¡Corre a tu posición en la colina!"
    
    al "¿En la colina del Pentélico?"
    
    pl "Justo ahí, al norte de la cantera."
    
    scene combatedelpentelico #Escenario por definir
    
    #Aquí habría que desarrollar una "mecánica" de combate de arco. Habría que abatir a 4 o 5 tíos y después se presentaría la decisión.
    
    al "A por ellos."
    
    jump decision_final
    
label combate_hoplita:
   
   pl "Esperaremos a que llegue la falange entera."
    
   pl "Combatiréis en la ladera del Parnés, al norte de Acarnas."
   
   al "Entendido."
   
   scene combatedelparnés #Escenario por definir
   
   #Aquí la mecánica sería distinta a la de arco. Habría 2-3 enemigos y sería una mecánica por turnos. Si mueres se haría jump hacia atrás para 
   #volver a intentar hasta que consigas la victoria.
   
   jump decision_final
   
label decision_final:
    
    al "Voy a por ese de allí."
    
    #EL objetivo se abalanza sobre un comandante ateniense.
    
    pl "¡Alejandro! ¡Auxilia al comandante"
    
    al "Pero... la historia..."
    
    menu:
        
       "Salvarlo":
          "Las normas de la agencia por el forro."
          jump cap5_salvarlo
          
       "La historia es la que es.":
          "¡Alejandro!¡Ven conmigo! - grita el polemarca."
          jump cap5_muere

   

   
   
