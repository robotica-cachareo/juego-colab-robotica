label cap5_salvarlo:
    
    pl "¡Vamos! Los espartanos se retiran."
    
    pl "Lo has hecho estupendo, Alejandro. Volvamos a Atenas."
    
    al "Antes, tengo que ir a..."
    
    al "A orinar, ahí en el descampado"
    
    pl "Vale, vale, aquí te espero."
    
    scene atenas_descampado #Escenario por realizar
    
    na "Alejandro pulsa el botón de su reloj y se comienza el proceso"
    
    mt "Atención, hemos detectado a un usuario de la red Arkadia en su misma época y lugar."
    
    mt "¿Desea incorporar a un segundo pasajero?"
    
    al "Es Sofía... Sí."
    
    mt "Preparando viaje, analizando código de tiempo, comprobando algoritmo.."
    
    mt "Veintitrés por ciento..."
    
    mt "Setenta y seis por ciento..."
    
    mt "Comprobación exitosa. Enlace establecido con Centro de Control."
    
    mt "Destino: Centro de Control de la Red Arkadia. 12 de julio de 2020."
    
    scene centro_de_control #Escenario por realizar
    
    al "Bien, ya estamos aquí. Tengo que comprobar que mis actos no han cambiado nada."
    
    so "¿Cambiado? ¿Qué has hecho en esa estúpida batalla?"
    
    al "Un tipo. Comandante ateniense."
    
    so "¿Lo mataste?"
    
    al "Al revés. Vamos al centro de investigación genealógica y veamos que ha pasado."
    
    so "OK."
    
    na "Alejandro y Sofía preparan el CENIT-GEN para investigar."
    
    cg "Inicializando sistema..."
    
    cg "Sistema activado, identifíquese."
    
    so "Agente especial Sofía, nº de IEATA 37972142"
    
    al "Agente especial Alejandro, nº 23726071"
    
    cg "Identificaciones correctas. Bienvenidos. ¿Qué desean?"
    
    so "Solicito comprobación tipo diff de backups anteriores al 11 de julio con datos actuales."
    
    cg "Revisando posibles errores... "
    
    al "Me parece que va a tardar un rato..."
    
    cg "Alerta detectada."
    
    so "¿Qué pasa?"
    
    cg "El sistema advierte la desaparición de un importante autor y su obra"
    
    al "¿Qué autor exactamente?"
    
    cg "Miguel de Cervantes."
    
    so "Vaya... ¿Cuál es el origen de la anomalía?"
    
    cg "Directamente, un asesinato en 1521."
    
    so "Pero Miguel nació más tarde..."
    
    al "¿Su padre?"
    
    cg "Exacto."
    
    so "Pues nada, a 1502. ¿Localización del suceso?"
    
    cg "40.478947, -3.348520"
    
    al "Pues nada, vamos para allá."
    
    na "Sofía coloca el código necesario para viajar."
    
    mt "Código correcto. Preparando viaje... Buscando lugar seguro de aterrizaje..."
    
    mt "Viaje en 3, 2 ,1..."
    
    scene alcaladehenares1502 #Escenario por definir
    
    so "Pues ya estamos aquí."
    
    al "España... ¿sabes si en 1502 ya hacían tortilla? Porq"
    
    so "No hemos venido a comer"
    
    al "Ya, bueno pero..."
    
    so "Pero nada. Vamos a buscar a Rodrigo."
    
    al "¿Rodrigo?"
    
    so "El padre de Cervantes."
    
    al "Ah."
    
    so "Debería vivir en aquella casita de allí."
    
    scene casitadecervantes #Escenario por definir
    
    al "¿Hola?"
    
    cp "Dígame, forastero."
    
    al "¿Es usted Rodrigo de Cervantes?"
    
    rg "Así es como me llaman."
    
    so "¿Le ha pasado algo extraño recientemente?"
    
    rg "¿A mí? Aparte de que las patatas me salgan pochas de la cosecha, no."
    
    al "¿Las patatas?¿Pero las patatas no vienen de América?"
    
    rg "¿América?¿Y dónde está eso si puede saberse?"
    
    rg "No, esto me lo trajo mi hermano de las Indias, de la travesía que hizo el año pasado."
    
    so "¿Su hermano estuvo en las Indias? Quién pudiera ir,  ¿verdad?"
    
    rg "Bueno, esas al menos se sabe donde están, no como la América de su marido."
    
    al "No, pero nos..."
    
    so "Sí, es que a veces escribe novelas sobre tierras misteriosas y a veces no distingue la realidad de la ficción."
    
    na "El tono le indica entre líneas a Alejandro que disimule."
    
    rg "Bueno, ¿que querían exactamente?"
    
    al "Lo de las cosas extrañas. ¿Alguien le ha seguido o...?"
    
    rg "En realidad, hace un par de días había un tipo en la taberna. Ese no es de por aquí, hablaba raro. Quizá venga de la Holanda o del Francocondado."
    
    al "¿Y cómo iba vestido?"
    
    rg "Así como con ropajes negros, y gorro."
    
    so "Bueno, iremos a la taberna por si está allí."
    
    rg "Pues eso, vayan con Dios."
    
    al "A usted."
    
    scene latabernadelpueblo #Escenario por definir
    
    al "Vaya pocilga. En Arcadia estos sitios llevan siglos sin existir."
    
    so "Alejandro, tú a veces estás intentando que me vuelva loca, ¿verdad?"
    
    al "¿Por qué lo dices?"
    
    so "¿Tú sabes dónde trabajas, no...?"
    
    al "Sí,p..."
    
    na "El hombre misterioso entra a la taberna."
    
    so "Oye, ese no es..."
    
    al "Exactamente el tipo que nos describió Rodrigo."
    
    so "Eh, tú."
    
    na "El tipo se gira y recibe un golpe en el mentón. Cae al suelo inconsciente."
    
    scene zulo1521 #Escenario por definir
    
    al "Hombre, por fin despiertas, capullo."
    
    hm "¿Dónde estoy?"
    
    so "Rodrigo de Cervantes."
    
    hm "Ese... quiénes sois."
    
    al "¿Qué le ibas a hacer?"
    
    hm "Hace dos días, un tipo... me dijo que lo matara, que le debía dinero."
    
    al "Pues vas a coger tus dos patitas y te vas a ir a matar al tipo."
    
    hm "¿Y por qué iba a hacer eso?"
    
    so "Porque si no lo haces, estás muerto."
    
    hm "Vale, vale."
    
    so "Tira, ya te puedes ir."
    
    na "Liberan al hombre misterioso."
    
    so "Otro gran trabajo de los mejores agentes de la historia."
    
    al "Gran trabajo es un poco optimista, ¿no crees?"
    
    so "Quizá sí. El caso es que está hecho. Volvamos al presente."
    
    al "Solicito 10-8."
    
    mt "10-8 concedido. Vuelta a agencia preparada. Ejecutando..."
    
    scene maquinatiempoagencia #Escenario por definir
    
    al "Vale. Ahora toca..."
    
    so "Gripe del 18. Texas."
    
    al "Pues venga... vamos para allí."
    
    so "MT, prepara viaje a Houston, 1918."
    
    mt "Preparando viaje... Tarea completada."
    
    mt "¿Desea ejecutar la orden?"
    
    al "Ejecútese"
    
    jump cap6
    
label cap5_muere:
    
    pl "¿Pero que coño haces?"
    
    al "Yo... que..."
    
    pl "¡AHORA ESTÁ MUERTO POR TU CULPA!"
    
    pl "¡TRAIDOR!"
    
    al "Pero..."
    
    pl "TÚ TE VIENES CONMIGO CAPULLO. A LA CÁRCEL."
    
    scene carcelateniense #Escenario por definir
    
    na "Alejandro coge la radio y la activa."
    
    al "Sofía... ¿me recibes?"
    
    so "¿Al-ja-o? ¿-e s-cuc-as?"
    
    al "Se oye mal."
    
    so "E.. era."
    
    so "Vale. ¿Ahora?"
    
    al "Mejor. Escucha... lo de la batalla se ha complicado."
    
    so "¿Complicado?"
    
    al "Es una larga historia... tienes que venir a la cárcel."
    
    so "¿A LA CÁRCEL?"
    
    al "Sí... mientras vine me fijé por donde iba, estoy al norte del Cerámico."
    
    al "Hay un par de guardias en mi visual, pero es probable que haya más."
    
    so "Vale...[suspira fuerte], voy para allá."
    
    scene carcelporfuera #Escenario por definir.
    
    #Esta parte la dejo en incógnita.
    #Quiero crear algún tipo de mecánica pero aún tengo que pensar como ejecutar la idea del rescate.
    
    scene esconditeatenas #Escenario por definir.
    
    so "Bueno, te he salvado la vida. Otra vez. Ya ni siquiera es novedad."
    
    al "Todavía tienes que remontar. Vamos 10-8."
    
    so "Lo de Amsterdam no cuenta como una. No había ningún peligro."
    
    so "Y la novena se supone que es... ¿Moscú? ¿Eso es salvar la vida? 8-8."
    
    al "Entonces esta tampoco cuenta, porque no me iban a matar."
    
    so "Claro que no, sólo hay una epidemia mortal en la ciudad, seguro que no te pasaba nada."
    
    so "Anda, vamos a ir a por la última cepa."
    
    al "¿1918?"
    
    so "Correcto."
    
    al "La pandemia surgió en Kansas, pero en San Antonio, Texas, se intensificó bastante. Fue unos años antes de que Arkadia derrotara a EEUU y asumiera el liderazgo del bloque del Oeste."
    
    so "Gracias por la clase de historia, señor Wikipedia, pero no tenemos tiempo para tonterías."
    
    so "MT, prepara viaje a San Antonio, 15 de mayo de 1918."
    
    mt "Preparando..."
    
    mt "Buscando lugar de aterrizaje..."
    
    mt "Ejecutando viaje..."
    
    jump cap6final
    
    
    


